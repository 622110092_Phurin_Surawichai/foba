using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SStageMultiPlayer : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button BackBut;
    [SerializeField] Button Stage1M;
    [SerializeField] Button Stage2M;
    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource> ();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];

        SetupButtonsDelegate();
        
        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop ();
        
        audiosourceButtonUI.PlayOneShot (audioclipHoldOver);
    }

    void SetupButtonsDelegate()
    {
        
        BackBut.onClick.AddListener(delegate { BackClick(BackBut); });
        Stage1M.onClick.AddListener(delegate { S1Click(Stage1M); });
        Stage2M.onClick.AddListener(delegate { S2Click(Stage2M); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackClick(Button button)
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    public void S1Click(Button button)
    {
        SceneManager.LoadScene("StageM 1");
    }
    
    public void S2Click(Button button)
    {
        SceneManager.LoadScene("StageM 2");
    }
  
}
