using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuControlScript2 : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button tutorialButton;
    [SerializeField] Button startButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Button exitButton;
    [SerializeField] Button MultiPlayerButton;
    
    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource> ();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];

        SetupButtonsDelegate();
        
        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop ();
        
        audiosourceButtonUI.PlayOneShot (audioclipHoldOver);
    }

    void SetupButtonsDelegate()
    {
        tutorialButton.onClick.AddListener(delegate { tutorialButtonClick(tutorialButton); });
        startButton.onClick.AddListener(delegate { StartButtonClick(startButton); });
        optionsButton.onClick.AddListener(delegate { OptionsButtonClick(optionsButton); });
        exitButton.onClick.AddListener(delegate { ExitButtonClick(exitButton); });
        MultiPlayerButton.onClick.AddListener(delegate { MultiPlayerButtonClick(MultiPlayerButton); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void tutorialButtonClick(Button button)
    {
        SceneManager.LoadScene("Level 0");
    }
    public void StartButtonClick(Button button)
    {
        SceneManager.LoadScene("StageSelect");
    }
    public void OptionsButtonClick(Button button)
    {
       
        SceneManager.LoadScene("Option");
    }

    public void ExitButtonClick(Button button)
    {
        Application.Quit();
    }

    public void MultiPlayerButtonClick(Button button)
    {
        SceneManager.LoadScene("MultiStageSelect");
    }
}
