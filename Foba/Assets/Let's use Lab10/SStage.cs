using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SStage : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] Button BackBut;
    [SerializeField] Button Stage1;
    [SerializeField] Button Stage2;
    [SerializeField] Button Stage3;
    [SerializeField] Button Stage4;
    [SerializeField] Button Stage5;
    [SerializeField] Button Stage6;
    [SerializeField] Button Stage7;
    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource> ();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];

        SetupButtonsDelegate();
        
        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop ();
        
        audiosourceButtonUI.PlayOneShot (audioclipHoldOver);
    }

    void SetupButtonsDelegate()
    {
        
        BackBut.onClick.AddListener(delegate { BackClick(BackBut); });
        Stage1.onClick.AddListener(delegate { S1Click(Stage1); });
        Stage2.onClick.AddListener(delegate { S2Click(Stage2); });
        Stage3.onClick.AddListener(delegate { S3Click(Stage3); });
        Stage4.onClick.AddListener(delegate { S1Click(Stage4); });
        Stage5.onClick.AddListener(delegate { S2Click(Stage5); });
        Stage6.onClick.AddListener(delegate { S3Click(Stage6); });
        Stage7.onClick.AddListener(delegate { S3Click(Stage7); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackClick(Button button)
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    public void S1Click(Button button)
    {
        SceneManager.LoadScene("Stage1");
    }
    
    public void S2Click(Button button)
    {
        SceneManager.LoadScene("Stage2");
    }
    
    public void S3Click(Button button)
    {
        SceneManager.LoadScene("Stage3");
    }
    public void S4Click(Button button)
    {
        SceneManager.LoadScene("Stage4");
    }
    public void S5Click(Button button)
    {
        SceneManager.LoadScene("Stage5");
    }
    public void S6Click(Button button)
    {
        SceneManager.LoadScene("Stage6");
    }
    public void S7Click(Button button)
    {
        SceneManager.LoadScene("Stage7");
    }
}
