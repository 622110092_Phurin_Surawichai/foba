using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio; 
public class playSFX : MonoBehaviour
{
    public AudioSource _source;
    public AudioClip _SFX;
    private void OnTriggerEnter(Collider other)
    {
        _source.PlayOneShot(_SFX);
    }
}
