using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OptionS : MonoBehaviour, IPointerEnterHandler
{
    
    [SerializeField] Button SButton;
    [SerializeField] Button bButton;
    [SerializeField] Toggle _toggleMusic;
    [SerializeField] Toggle _toggleSFX;
    
    AudioSource audiosourceButtonUI;
    [SerializeField] AudioClip audioclipHoldOver;
    // Start is called before the first frame update
    void Start()
    {
        this.audiosourceButtonUI = this.gameObject.AddComponent<AudioSource> ();
        this.audiosourceButtonUI.outputAudioMixerGroup = SingletonSoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];
        _toggleMusic.isOn = SingletonGameApplicationManager.Instance.MusicEnabled;
        _toggleSFX.isOn = SingletonGameApplicationManager.Instance.SFXEnabled;
        _toggleMusic.onValueChanged.AddListener(delegate{ OnToggleMusic(_toggleMusic);});
        _toggleSFX.onValueChanged.AddListener(delegate{ OnToggleSFX(_toggleSFX);});
        SetupButtonsDelegate();
        
        if (!SingletonSoundManager.Instance.BGMSource.isPlaying)
            SingletonSoundManager.Instance.BGMSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (audiosourceButtonUI.isPlaying)
            audiosourceButtonUI.Stop ();
        
        audiosourceButtonUI.PlayOneShot (audioclipHoldOver);
    }

    void SetupButtonsDelegate()
    {
        SButton.onClick.AddListener(delegate { SButtonClick(SButton); });
        bButton.onClick.AddListener(delegate { BButtonClick(bButton); });
        
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SButtonClick(Button button)
    {
        SceneManager.LoadScene("StageSelect");
    }
    
    public void BButtonClick(Button button)
    {
       
            SceneManager.LoadScene("MainMenu");
    }

    public void OnToggleMusic(Toggle toggle)
    {
        SingletonGameApplicationManager.Instance.MusicEnabled = _toggleMusic.isOn;
        if(SingletonGameApplicationManager.Instance.MusicEnabled)
            SingletonSoundManager.Instance.MusicVolume = SingletonSoundManager.Instance.MusicVolumeDefault;
        else
            SingletonSoundManager.Instance.MusicVolume = SingletonSoundManager.MUTE_VOLUME;
    }

    public void OnToggleSFX(Toggle toggle)
    {
        SingletonGameApplicationManager.Instance.SFXEnabled = _toggleSFX.isOn;
        if(SingletonGameApplicationManager.Instance.SFXEnabled)
            SingletonSoundManager.Instance.MasterSFXVolume = SingletonSoundManager.Instance.MasterSFXVolumeDefault;
        else
            SingletonSoundManager.Instance.MasterSFXVolume = SingletonSoundManager.MUTE_VOLUME;
            
        
    }
    
}
