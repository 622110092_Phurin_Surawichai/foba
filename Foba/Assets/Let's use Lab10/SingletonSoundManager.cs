using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class SingletonSoundManager : Singleton<SingletonSoundManager>
{
    public const float MUTE_VOLUME = -80;

    [SerializeField]
    protected AudioMixer mixer;
    public AudioClip[] StageMusic; //��ɫҡ�ҡ��þ���������¹���§
    public AudioMixer Mixer { get { return mixer;} set { mixer = value; } }   
    [SerializeField]public AudioSource BGMSource{get;set;}

    protected Scene currentScene; //��ɫҡ�ҡ��þ���������¹���§
    protected string sceneName; //��ɫҡ�ҡ��þ���������¹���§

    #region Music Volume

    public float MusicVolumeDefault{get;set;}    

    protected float musicVolume;
    public float MusicVolume
    {
        get
        {
            return this.musicVolume;
        }
        set
        {
            this.musicVolume = value;
            SingletonSoundManager.Instance.Mixer.SetFloat("MusicVolume", musicVolume);
        }
    }

    #endregion

    #region SFX Volume
    protected float masterSFXVolume;
    public float MasterSFXVolume
    {
        get
        {
            return this.masterSFXVolume;
        }
        set
        {
            this.masterSFXVolume = value;
            SingletonSoundManager.Instance.Mixer.SetFloat("MasterSFXVolume", this.masterSFXVolume);
        }
    }
    

    public float MasterSFXVolumeDefault { get;set; }


    #endregion

    //�Ⱥ�ҡ�ҡ��þ���������¹���§
    private void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        OnEnable();
    }

    public override void Awake()
    {
        base.Awake();
        
        this.BGMSource = this.GetComponent<AudioSource>();

        float musicVolume;
        if (mixer.GetFloat("MusicVolume", out musicVolume))
            Instance.MusicVolumeDefault = musicVolume;
        float masterSFXVolume;
        if (mixer.GetFloat("MasterSFXVolume", out masterSFXVolume))
            Instance.MasterSFXVolumeDefault = masterSFXVolume;

    }

    //�Ⱥ�ҡ�ҡ��þ���������¹���§
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        
    }

    private void OnDisable()
    {
        //BGMSource.Stop();
        SceneManager.sceneLoaded -= OnSceneLoaded;
        
    }

    public void OnSceneLoaded(Scene scene,LoadSceneMode mode)
    {
        switch (scene.buildIndex)
        {
            case 0:
                BGMSource.clip = StageMusic[0];
                
                break;
            case 11:
                BGMSource.clip = StageMusic[1];
                
                break;
            case 12:
                BGMSource.clip = StageMusic[2];
                break;
            case 13:
                BGMSource.clip = StageMusic[3];
                break;
            case 14:
                BGMSource.clip = StageMusic[4];
                break;
            case 15:
                BGMSource.clip = StageMusic[5];
                break;
            case 16:
                BGMSource.clip = StageMusic[6];
                break;
            case 17:
                BGMSource.clip = StageMusic[7];
                break;
            case 18:
                BGMSource.clip = StageMusic[8];
                break;
            case 19:
                BGMSource.clip = StageMusic[9];
                break;
            case 20:
                BGMSource.clip = StageMusic[10];
                break;
            case 10:
                BGMSource.clip = StageMusic[11];
                break;
        }
        BGMSource.Play();
    }
}
