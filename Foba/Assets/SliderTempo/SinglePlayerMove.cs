using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.InputSystem;
public class SinglePlayerMove : MonoBehaviour
{
    public Slider Tempo;

    public float min = -100;
    public float max = 100;

  
    

    protected bool front, behind, left, right;
    
    void Start()
    {
        
        front = true;
        behind = false;
        left = false;
        right = false;
       
    }
    void Update()
    {
        
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            if (Input.GetKeyDown(KeyCode.Q))
            {
               
                transform.Rotate(0, -90, 0);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
               
                transform.Rotate(0, 90, 0);
            }
            if (Tempo.value > min && Tempo.value < max)
            {
                if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) ||
                    Input.GetKeyUp(KeyCode.S))
                {
                    ToMoveTrue();
                }

            }
            if (Tempo.value < min || Tempo.value > max)
            {
                if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) ||
                    Input.GetKeyUp(KeyCode.S))
                {
                    ToMoveFalse();
                }
            }
         
    }

    
    public void ToMoveTrue()
    {
        //True Right
        if (Input.GetKeyUp(KeyCode.D))
        {
            Vector3 horidirection = new Vector3(1, 0f, 0f);
            transform.Translate(horidirection);
        }
        //True Left
        if (Input.GetKeyUp(KeyCode.A))
        {
            Vector3 horidirection = new Vector3(-1, 0f, 0f);
            transform.Translate(horidirection);
        }
        //True Up
        if (Input.GetKeyUp(KeyCode.W))
        {
            Vector3 vertidirection = new Vector3(0f, 0f, 1);
            transform.Translate(vertidirection);
        }
        //True Down
        if (Input.GetKeyUp(KeyCode.S))
        {
            Vector3 vertidirection = new Vector3(0f, 0f, -1);
            transform.Translate(vertidirection);
        }
    }
    public void ToMoveFalse()
    {
        //False Right
        if (Input.GetKeyUp(KeyCode.D))
        {
            Vector3 horidirection = new Vector3(-1, 0f, 0f);
            transform.Translate(horidirection);
        }
        //False Left
        if (Input.GetKeyUp(KeyCode.A))
        {
            Vector3 horidirection = new Vector3(1, 0f, 0f);
            transform.Translate(horidirection);
        }
        //False Up
        if (Input.GetKeyUp(KeyCode.W))
        {
            Vector3 vertidirection = new Vector3(0f, 0f, -1);
            transform.Translate(vertidirection);
        }
        //False Down
        if (Input.GetKeyUp(KeyCode.S))
        {
            Vector3 vertidirection = new Vector3(0f, 0f, 1);
            transform.Translate(vertidirection);
        }
    }

    public void OnTurning()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
               
            transform.Rotate(0, -90, 0);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
               
            transform.Rotate(0, 90, 0);
        }
    }
}