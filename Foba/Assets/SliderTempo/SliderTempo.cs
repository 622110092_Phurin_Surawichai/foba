using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class SliderTempo : MonoBehaviour
{
    public Slider sliderTempo;
    public float sliderSpeedPerSecond = 10;
    public AudioSource queueSFX;
    public enum TEMPO_DIRECTION
    {
        POSITIVE,NEGATIVE
    }
    public TEMPO_DIRECTION tempoDirection = TEMPO_DIRECTION.POSITIVE;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch(tempoDirection)
        {
            case TEMPO_DIRECTION.POSITIVE:
                sliderTempo.value += sliderSpeedPerSecond * Time.deltaTime;
                if (sliderTempo.value > -101 && sliderTempo.value < -99)
                {
                    queueSFX.Play();
                }
                if (sliderTempo.value>=sliderTempo.maxValue)
                {
                    tempoDirection = TEMPO_DIRECTION.NEGATIVE;
                }
                break;
            case TEMPO_DIRECTION.NEGATIVE:
                sliderTempo.value -= sliderSpeedPerSecond * Time.deltaTime;
                if (sliderTempo.value > 99 && sliderTempo.value < 101)
                {
                    queueSFX.Play();
                }
                if(sliderTempo.value<=sliderTempo.minValue)
                {
                    tempoDirection = TEMPO_DIRECTION.POSITIVE;
                }
                break;
        }
        
    }
}
