using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class setTempoValue : MonoBehaviour
{
    public Slider Tempo;
    public float minvalue;
    public float maxvalue;
    // Start is called before the first frame update
    void Start()
    {
        Tempo.minValue = minvalue;
        Tempo.maxValue = maxvalue;
    }
    private void OnLevelWasLoaded(int level)
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
