using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playermovingMKII : MonoBehaviour
{
    public bool normalPhase = true;
    public Slider Tempo;

    public float min = -50;
    public float max = 50;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

     

       //true turn true
        if ((Tempo.value>min && Tempo.value<max)&& normalPhase == true)
        {
            if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                ToMoveTrue();
                normalPhase = true;
            }

        }
      //true turn false
        if ((Tempo.value < min || Tempo.value > max)&& normalPhase == true)
        {
            if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                ToMoveFalse();
                normalPhase = false;
            }
        }

        //false turn false
        if ((Tempo.value > min && Tempo.value < max) && normalPhase == false)
        {
            if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                ToMoveFalse();
                normalPhase = false;
            }

        }
        //false turn true
        if ((Tempo.value < min || Tempo.value > max) && normalPhase == false)
        {
            if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                ToMoveTrue();
                normalPhase = true;
            }
        }
    }
    public void ToMoveTrue()
    {
        //if (normalPhase == true)
        //{
            //True Right
            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                Vector3 horidirection = new Vector3(1, 0f, 0f);
                transform.position += horidirection;
                
            }

            //True Left
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                Vector3 horidirection = new Vector3(-1, 0f, 0f);
                transform.position += horidirection;
                
            }

            //True Up
            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                Vector3 vertidirection = new Vector3(0f, 0f, 1);
                transform.position += vertidirection;
                
            }

            //True Down
            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                Vector3 vertidirection = new Vector3(0f, 0f, -1);
                transform.position += vertidirection;
               
            }
        //}
        
    }
    public void ToMoveFalse()
    {
        //if (normalPhase == false)
        //{
            //False Right
            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                Vector3 horidirection = new Vector3(-1, 0f, 0f);
                transform.position += horidirection;

            }

            //False Left
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                Vector3 horidirection = new Vector3(1, 0f, 0f);
                transform.position += horidirection;

            }

            //False Up
            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                Vector3 vertidirection = new Vector3(0f, 0f, -1);
                transform.position += vertidirection;

            }

            //False Down
            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                Vector3 vertidirection = new Vector3(0f, 0f, 1);
                transform.position += vertidirection;

            }
        //}
    }
}
