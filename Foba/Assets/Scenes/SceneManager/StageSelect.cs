using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StageSelect : MonoBehaviour
{
    public void Level_1()
    {
        SceneManager.LoadScene("Stage 1");
    }
    public void Level_2()
    {
        SceneManager.LoadScene("Stage 2");
    }
    public void Level_3()
    {
        SceneManager.LoadScene("Stage 3");
    }
    public void Level_4()
    {
        SceneManager.LoadScene("Stage 4");
    }
    public void Level_5()
    {
        SceneManager.LoadScene("Stage 5");
    }
    public void Level_6()
    {
        SceneManager.LoadScene("Stage 6");
    }
    public void Level_7()
    {
        SceneManager.LoadScene("Stage 7");
    }
    public void Level_1M()
    {
        SceneManager.LoadScene("StageM 1");
    }
    public void Level_2M()
    {
        SceneManager.LoadScene("StageM 2");
    }
    public void BackToMainMenu ()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void ToEnd()
    {
        SceneManager.LoadScene("THE END");
    }
}
