using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class UniversalStageChange : MonoBehaviour
{
    public string stageName = "";
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene(stageName);
    }

    public void ChangeStageByName()
    {
        SceneManager.LoadScene(stageName);
    }
}
