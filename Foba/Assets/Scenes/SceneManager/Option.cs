using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Option : MonoBehaviour
{
    public void GoStageSelect()
    {
        SceneManager.LoadScene("StageSelect");
    }
    public void BackToMainMenu ()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
