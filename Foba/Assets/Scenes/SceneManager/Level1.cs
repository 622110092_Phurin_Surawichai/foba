using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class Level1 : MonoBehaviour
{
    
    public AudioSource BGMSource { get; set; }

    public void Restart1()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void Restart2()
    {
        SceneManager.LoadScene("Level2");
    }
    public void Restart3()
    {
        SceneManager.LoadScene("Level3");
    }
    public void GoStageSelect()
    {
        SceneManager.LoadScene("StageSelect");
    }

    public void GoOption()
    {
        SceneManager.LoadScene("Option");
    }
    public void GoMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void AdditiveOption()
    {
        SceneManager.LoadScene("AdditiveOption",LoadSceneMode.Additive);
    }
    public void AdditiveOptionMultiPlayer()
    {
        SceneManager.LoadScene("AdditiveOptionMultiPlayer",LoadSceneMode.Additive);
    }
    public void AdditiveStageSelect()
    {
        SceneManager.LoadScene("AdditiveStageSelect",LoadSceneMode.Additive);
    }
    public void AdditiveStageSelectMultiPlayer()
    {
        SceneManager.LoadScene("AdditiveMultiStageSelect",LoadSceneMode.Additive);
    }
    public void AdditiveMenu()
    {
        SceneManager.LoadScene("AdditiveMenu",LoadSceneMode.Additive);
    }
    public void AdditiveMenuMultiplayer()
    {
        SceneManager.LoadScene("AdditiveMenuMultiPlayer",LoadSceneMode.Additive);
    }
    public void CloseAdditiveOption()
    {
        SceneManager.UnloadSceneAsync("AdditiveOption");
    }
    public void CloseAdditiveOptionMultiPlayer()
    {
        SceneManager.UnloadSceneAsync("AdditiveOptionMultiPlayer");
    }
    public void CloseAdditiveStageSelect()
    {
        SceneManager.UnloadSceneAsync("AdditiveStageSelect");
    }
    public void CloseAdditiveStageSelectMultiPlayer()
    {
        SceneManager.UnloadSceneAsync("AdditiveStageSelectMultiPlayer");
    }
    public void CloseAdditiveMenu()
    {
        SceneManager.UnloadSceneAsync("AdditiveMenu");
    }
    public void CloseAdditiveMenuMultiplayer()
    {
        SceneManager.UnloadSceneAsync("AdditiveMenuMultiPlayer");
    }
}
