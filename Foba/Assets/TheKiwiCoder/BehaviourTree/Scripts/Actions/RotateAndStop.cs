using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class RotateAndStop : ActionNode
{
    
    public float rotationSpeed = 60;
    private float rotateTime;
    private bool clockWise = false;

    private void RandomRotationParams()
    {
        rotateTime = 3;
        float rnd = Random.Range(0, 99);
        if (rnd > 50)
        {
            clockWise = true;
        }
        else
        {
            clockWise = false;
        }
    }
    protected override void OnStart()
    {
        RandomRotationParams();
    }
    protected override void OnStop() 
    {
        
    }

    protected override State OnUpdate()
    {
        float rotationDirectionMultiplier = 1;
        if (clockWise)
        {
            rotationDirectionMultiplier = 1;
        }
        else
        {
            rotationDirectionMultiplier = -1;
        }
        context.gameObject.transform.Rotate(new Vector3(0,1,0),rotationSpeed*rotationDirectionMultiplier*Time.deltaTime,Space.Self);
        rotateTime -= Time.deltaTime;
        if (rotateTime <= 0)
        {
            return State.Failure;
        }
        return State.Running;
    }
}
