using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class SpaceCheckNode : ActionNode
{
    public Vector3 startingLocation;
    protected override void OnStart() 
    {
        startingLocation = this.position;
    }

    protected override void OnStop() 
    {
    }

    protected override State OnUpdate() 
    {
        //x
        if (this.position3D.x < startingLocation.x - 10)
        {
            blackboard.moveToPosition.x = startingLocation.x-9;
        }
        if (this.position3D.x > startingLocation.x + 10)
        {
            blackboard.moveToPosition.x = startingLocation.x+9;
        }
        //y
        if (this.position3D.y < startingLocation.y - 10)
        {
            blackboard.moveToPosition.y = startingLocation.y-9;
        }
        if (this.position3D.y > startingLocation.y + 10)
        {
            blackboard.moveToPosition.y = startingLocation.y+9;
        }
        //z
        if (this.position3D.z < startingLocation.z - 10)
        {
            blackboard.moveToPosition.z = startingLocation.z-9;
        }
        if (this.position3D.z > startingLocation.z + 10)
        {
            blackboard.moveToPosition.z = startingLocation.z+9;
        }
        return State.Success;
    }
}
