using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mazerotate : MonoBehaviour
{
    public GameObject _maze;
    public GameObject _maze2;
    public GameObject _maze3;
    public GameObject _maze4;
    public GameObject _this;
    // Start is called before the first frame update
    private void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {  
        _maze.transform.Rotate(0.0f, 90.0f, 0.0f,Space.Self);
        _maze2.transform.Rotate(0.0f, 90.0f, 0.0f,Space.Self);
        _maze3.transform.Rotate(0.0f, 90.0f, 0.0f,Space.Self);
        _maze4.transform.Rotate(0.0f, 90.0f, 0.0f,Space.Self);
        _this.gameObject.SetActive(false);
    }
}
