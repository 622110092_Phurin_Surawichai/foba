using System;
using UnityEngine;
using Photon.Pun; 
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;

public class PunNetworkManager : ConnectAndJoinRandom 
{
    public static PunNetworkManager singleton;
    public bool isUseMainCamera;
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    public GameObject playerSpawn1;
    public GameObject playerSpawn2;
    
    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public bool isGameOver = false;

    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    
    private void Awake()
    {
        singleton = this;
        
        //Add Reference Method to Delegate Method
        OnPlayerSpawned += SpawnPlayer;
        
        //When Connected from Lancher Scene
        if (PhotonNetwork.IsConnected)
        {
            this.AutoConnect = false;
            OnJoinedRoom();
        }
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);

        Debug.Log("New Player. " + newPlayer.ToString());
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        if (isUseMainCamera == false)
            Camera.main.gameObject.SetActive(isUseMainCamera);

        OnPlayerSpawned();
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        if (isUseMainCamera == false) 
            Camera.main.gameObject.SetActive(!isUseMainCamera);
    }

    /*public override void OnLeftRoom()
    {
        SceneManager.LoadScene("");
    }*/
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        object isGameOver;
        if (propertiesThatChanged.TryGetValue(PunGameSetting.GAMEOVER, out isGameOver))
        {
            Debug.Log("GAMEOVER Prop is : " + isGameOver); 
            LeaveRoom();
        }
    }
    public void LeaveRoom()
    {
        
        PhotonNetwork.LeaveRoom();
        
    }
    public void SpawnPlayer()
    {
        playerSpawn1 = GameObject.FindGameObjectWithTag("spawn1");
        playerSpawn2 = GameObject.FindGameObjectWithTag("spawn2");
        

        // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        if (PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"]!=null)
        {
            if (PunFPCUserNetControl.LocalPlayerInstance == null)
            {
                if ((int)PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"]==0)
                {
                    PhotonNetwork.Instantiate(GamePlayerPrefab.name, /*new Vector3(0f, 0.5f, 0f)*/
                        playerSpawn1.transform.position, Quaternion.identity, 0);
                    isGameStart = true;
                }
                else if ((int)PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"]==1)
                {
                    PhotonNetwork.Instantiate(GamePlayerPrefab.name, /*new Vector3(3.5f, 0.5f, 0f)*/
                        playerSpawn2.transform.position, Quaternion.identity, 0);
                    isGameStart = true;
                }
            }
        }
        else
        {
            PhotonNetwork.LocalPlayer.CustomProperties.Add("PLAYER_ID",PhotonNetwork.CountOfPlayersInRooms);

            if (PunFPCUserNetControl.LocalPlayerInstance == null)
            {
                if ((int)PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"]==0)
                {
                    PhotonNetwork.Instantiate(GamePlayerPrefab.name, /*new Vector3(0f, 0.5f, 0f)*/
                        playerSpawn1.transform.position, Quaternion.identity, 0);
                    isGameStart = true;
                }
                else if ((int)PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"]==1)
                {
                    PhotonNetwork.Instantiate(GamePlayerPrefab.name, /*new Vector3(3.5f, 0.5f, 0f)*/
                        playerSpawn2.transform.position, Quaternion.identity, 0);
                    isGameStart = true;
                }
            }
        }
    }

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
            return;
    }
}
