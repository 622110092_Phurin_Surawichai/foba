using System;
using UnityEngine;

[Serializable]
public class GamePreferencesData
{
    public string _playerName;
    public int _score;
    public float _musicVolume;
    public float _SFXVolume;
    public Vector3 _theGameObject;
    public int _gpdM;
    public int _gpdS;
}