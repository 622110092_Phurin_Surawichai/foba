using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;
using UnityEngine.UI;

public class SaveLoadGamePreferencesDataManager : MonoBehaviour
{
    //[SerializeField]private TMP_InputField _inputFieldPlayerName, _inputFieldScore, _inputFieldFileName;
    [SerializeField] private Button _saveButton, _loadButton;
    //[SerializeField]private Slider _sliderMusicVolume, _sliderSFXVolume;
    //[SerializeField]TMP_Dropdown _dropdownSaveType;
    private string _saveFolderLocation;
    //[SerializeField] private GameObject _gameObject;
    [SerializeField]
    private Toggle _musicOn , _sfxOn;

    private int mCheck = 0,sCheck=0;

    [SerializeField]
    private string _fileName="save0";
    void Start()
    {
        _saveButton.onClick.AddListener(OnSaveClick);
        _loadButton.onClick.AddListener(OnLoadClick);
        if(!Directory.Exists("Saves"))
            Directory.CreateDirectory("Saves");
        _saveFolderLocation ="Saves";
    }

    void Update()
    {
        
    }
    void OnSaveClick() 
    {
        if(_musicOn.isOn)
        {
            mCheck = 1;
        }
        else
        {
            mCheck = 0;
        }

        if (_sfxOn.isOn)
        {
            sCheck = 1;
        }
        else
        {
            sCheck = 0;
        }
        GamePreferencesData gdp = new GamePreferencesData
        {
            //_playerName = _inputFieldPlayerName.text,
            //_score =int.Parse(_inputFieldScore.text),
            //_musicVolume = _sliderMusicVolume.value,
            //_SFXVolume = _sliderSFXVolume.value,
            //_theGameObject = _gameObject.transform.position
            _gpdM = mCheck,
            _gpdS = sCheck
                
        };
        //string filename = _inputFieldFileName.text;
        string location = Path.Combine(_saveFolderLocation, _fileName);
        ISaveLoadGamePreferencesData islg =null;
        /*switch(_dropdownSaveType.value) {
            case 0://XML
                  islg = new SaveLoadGamePreferencesDataXML();
                  location = location +".xml";
                  break; 
            case 1://JSON
                   islg = new SaveLoadGamePreferencesDataJSON();
                   location = location +".json";
                   break;
            case 2://Binary
                   islg = new SaveLoadGamePreferencesDataBinary();
                   location = location +".bin";
                   break;
            
        }*/
        islg = new SaveLoadGamePreferencesDataJSON();
        location = location +".json";
        islg.SaveGamePreferencesData(gdp, location);
    }
    void OnLoadClick()
    {
        //string filename = _inputFieldFileName.text;
        string location = Path.Combine(_saveFolderLocation, _fileName);
        ISaveLoadGamePreferencesData islg =null;
        /*switch(_dropdownSaveType.value)
        {
            case 0://XML
                   islg = new SaveLoadGamePreferencesDataXML();
                   location = location +".xml";
                   break;
            case 1://JSON
                   islg = new SaveLoadGamePreferencesDataJSON();
                   location = location +".json";
                   break;
            case 2://Binary
                   islg = new SaveLoadGamePreferencesDataBinary();
                   location = location +".bin";
                   break;
        }*/
        islg = new SaveLoadGamePreferencesDataJSON();
        location = location +".json";
        
        GamePreferencesData gdp = new GamePreferencesData();
        gdp = islg.LoadGamePreferencesData(location);
        //_inputFieldPlayerName.text = gdp._playerName;
        //_inputFieldScore.text = gdp._score.ToString();
        //_sliderMusicVolume.value= gdp._musicVolume;
        //_sliderSFXVolume.value= gdp._SFXVolume;
        //_gameObject.transform.position = gdp._theGameObject;
        
        //toggle here *****************
        mCheck = gdp._gpdM;
        sCheck = gdp._gpdS;
        if(mCheck==1)
        {
            _musicOn.isOn = true;
        }
        else
        {
            _musicOn.isOn = false;
        }

        if (sCheck==1)
        {
            _sfxOn.isOn = true;
        }
        else
        {
            _sfxOn.isOn = false;
        }
    }
}


