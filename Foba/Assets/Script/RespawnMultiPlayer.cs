using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun; 
using Photon.Pun.UtilityScripts;
using Photon.Realtime;

public class RespawnMultiPlayer : MonoBehaviourPun
{
    public float DeathPoint;

    public GameObject SpawnPoint1;

    public GameObject SpawnPoint2;
    
    // Start is called before the first frame update
    void Start()
    {
        SpawnPoint1 = GameObject.FindGameObjectWithTag("spawn1");
        SpawnPoint2 = GameObject.FindGameObjectWithTag("spawn2");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.position.y < DeathPoint)
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"]==0)
            {
                this.gameObject.transform.position = SpawnPoint1.transform.position;
            }
            else if ((int) PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"] == 1)
            {
                this.gameObject.transform.position = SpawnPoint2.transform.position;
            }
        }
    }
}
