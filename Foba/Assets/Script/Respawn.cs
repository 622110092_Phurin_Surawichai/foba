using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public GameObject Player;
    public GameObject RespawnPoint;
    public float DeathPoint;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.transform.position.y < DeathPoint)
        {
            Player.transform.position = RespawnPoint.transform.position;
        }

        //if (Input.GetKeyUp(KeyCode.P)){RespawnPoint = new Vector3(8, 0, -9);}
    }
}
