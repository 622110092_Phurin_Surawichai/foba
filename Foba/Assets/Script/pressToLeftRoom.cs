using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun; 
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine.SceneManagement;
public class pressToLeftRoom : MonoBehaviour
{
    public string stageName = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PressNext()
    {
        
        PhotonNetwork.LeaveRoom();
    }
    public void ChangeStageByName()
    {
        SceneManager.LoadScene(stageName);
    }
}
