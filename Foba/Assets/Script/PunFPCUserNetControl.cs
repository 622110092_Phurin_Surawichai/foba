using UnityEngine;
using StarterAssets;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class PunFPCUserNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback {
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public Transform CameraRoot;

    public void OnPhotonInstantiate(PhotonMessageInfo info) {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine) {
            LocalPlayerInstance = gameObject;
            GetComponentInChildren<MeshRenderer>().material.color = Color.red;

            PunNetManagerVCam PunNMVCam = (PunNetManagerVCam)PunNetworkManager.singleton;
            if (PunNMVCam != null)
            {
                PunNMVCam._vCam.Follow = CameraRoot;
                PunNMVCam._vCam.LookAt = CameraRoot;
            }
            
            else Debug.LogWarning("PunNetManagerVCam is NULL.");
        }
        else {
            GetComponent<playermovingMKIII>().enabled = false;
        }
    }
}
