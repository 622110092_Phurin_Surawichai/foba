using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

[CreateAssetMenu(fileName ="ScoreScriptable", menuName ="Foba/ScoreScriptable", order= 1)]
public class ScoreScriptable : ScriptableObject
{
    public int player1score;
    public int player2score;
}
