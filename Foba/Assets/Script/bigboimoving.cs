using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bigboimoving : MonoBehaviour
{
    Rigidbody _rb;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float negativehorizontal = Input.GetAxis("Horizontal");
        float negativevertical = Input.GetAxis("Vertical");


        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            Vector3 horidirection = new Vector3(15, 0f, 0f);
            transform.position += horidirection;
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Vector3 horidirection = new Vector3(-15, 0f, 0f);
            transform.position += horidirection;
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Vector3 vertidirection = new Vector3(0f, 0f, 15);
            transform.position += vertidirection;
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            Vector3 vertidirection = new Vector3(0f, 0f, -15);
            transform.position += vertidirection;
        }
    }
}
