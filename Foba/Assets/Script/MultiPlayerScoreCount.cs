using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun; 
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine.UI;
public class MultiPlayerScoreCount : MonoBehaviour
{
    public ScoreScriptable thisPersonScore;
    public Slider tempo;
    public Slider ComboBreakSlider;
    public Text TextScore;
    public Text MathModifierUI; //not sure it should be text?
    public Text finalScore1;
    public Text finalScore2;
    public float minfromplayer;
    public float maxfromplayer;
    public float ComboDecreasePerSecond;

    public SliderTempo callSpeed;
    public float plusSpeed=20;
    public float _sumCollectedSpeed=0;
    
    public int TempScore; //for math
    public int ScoreModifier;
    public int Score;
    
    public int casenum;
    public int HitCount;
    // Start is called before the first frame update
    void Start()
    {
        
        TempScore = 1;
        HitCount = 0;
        ComboBreakSlider.value = 0;
    }
    
    void Update()
    {
        ComboBreakSlider.value -= ComboDecreasePerSecond*Time.deltaTime;
        //Plus Score Combo
        if (tempo.value > minfromplayer && tempo.value < maxfromplayer)
        {
            
            if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
            {
                HitCount += 1;
                
                ComboBreakSlider.value = 300;
                
                //Combo();
                
                TempScore = 1;
                MathScore();

                callSpeed.sliderSpeedPerSecond += plusSpeed;
                _sumCollectedSpeed += plusSpeed;
            }
        }
        //Reset Score Combo on wrong phase
        else if(tempo.value < minfromplayer || tempo.value > maxfromplayer)
        {
            if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
            {
                HitCount = 0;
                ComboBreakSlider.value = 0;
                TempScore = 0;
                MathScore();
                callSpeed.sliderSpeedPerSecond -= _sumCollectedSpeed;
                _sumCollectedSpeed = 0;
            }
            
        }
        //Reset Score Combo on timeout
        if(ComboBreakSlider.value<0.1f)
        {
            HitCount = 0;
            ComboBreakSlider.value = 0;
            TempScore = 0;
            MathScore();
            callSpeed.sliderSpeedPerSecond -= _sumCollectedSpeed;
            _sumCollectedSpeed = 0;
        }
        TextScore.text = Score.ToString();
        finalScore1.text = Score.ToString();
        if ((int) PhotonNetwork.LocalPlayer.CustomProperties["PLAYER_ID"] == 1)
        {
            
        }
        finalScore2.text = Score.ToString();
    }

    void MathScore()
    {
        if (HitCount < 2)
        {
            casenum = 0;
        }
        else if (HitCount >= 2 && HitCount <= 4)
        {
            casenum = 1;
        }
        else if(HitCount>4)
        {
            casenum = 2;
        }
        /*else if (ComboBreakSlider.value == 0 || HitCount==0)
        {
            casenum = 3;
        }*/
        switch (casenum)
        {
            case 0 :
                ScoreModifier = 1;
                MathModifierUI.text = "x 1";
                break;
            case 1 :
                ScoreModifier = 2;
                MathModifierUI.text = "x 2";
                break;
            case 2 :
                ScoreModifier = 5;
                MathModifierUI.text = "x 5";
                break;
            /*case 3 :
                ScoreModifier = 1;
                MathModifierUI.text = "x 1";
                break;*/
            default:
                Debug.Log("ScoreModifier = default");
                break;
        }
        TempScore = TempScore * ScoreModifier;
        Score = Score + TempScore;
        
    }

    /*void Combo()
    {
        
        if (HitCount > 0 )
        {
            if (_timer < ComboBreakTime)
            {
                _timer += Time.deltaTime;
                ComboBreakSlider.value -= ComboDecreasePerSecond*Time.deltaTime;
            }
            else
            {
                HitCount = 0;
            }
            
        }
        
    }*/
}
