using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moving : MonoBehaviour
{
    private float movespeed = 100;
    private Vector3 direction = Vector3.right;
    // Start is called before the first frame update
    private void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * movespeed * Time.deltaTime);
        
    }
}
