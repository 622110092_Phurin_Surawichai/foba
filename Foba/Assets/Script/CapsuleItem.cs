using UnityEngine.Events;
using UnityEngine;

public class CapsuleItem : MonoBehaviour
{
    [SerializeField] protected UnityEvent onPickedUp;
    private void OnTriggerEnter(Collider other)
    { 
        if (other.gameObject.CompareTag("Player")) 
        { 
            onPickedUp.Invoke(); 
            
        } 
    }
}
