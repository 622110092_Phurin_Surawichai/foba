using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class unlockedTheGate : MonoBehaviour
{
    public GameObject gate;
    public GameObject checkOutSign;
    protected Vector3 padPosition;
    public GameObject associatedGateImage;
    private int checkoutCount = 0;
    private void Start()
    {
        padPosition = new Vector3(this.gameObject.transform.position.x,this.gameObject.transform.position.y+1,this.gameObject.transform.position.z);
        associatedGateImage.GetComponent<RawImage>();
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        gate.gameObject.SetActive(false);
        if (checkoutCount < 1)
        {
            Instantiate(checkOutSign,padPosition,Quaternion.identity);
        }
        checkoutCount += 1;
        associatedGateImage.SetActive(false);
    }

    private void RemoveLockedFromList()
    {
        //LockedGates.
    }
    // Update is called once per frame
    
}
